module.exports = {
  lintOnSave: false,
  baseUrl: process.env.NODE_ENV === 'production' ? '.' : '/',
  devServer: {
    proxy: 'http://192.168.0.116'
    //proxy: 'http://192.168.0.133'
  }
}
